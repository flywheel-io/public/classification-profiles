# classification-profiles

Repository to store classification profiles for
[fw-classification](https://gitlab.com/flywheel-io/scientific-solutions/lib/fw-classification).

## How to modify the classification schema

Currently, Flywheel supports classification of a few modalities: MR, PT, MG and CT.

Other modalities can be included, and thus classified, levaraging the pre-defined **schema**
structured in Flywheel. The keywords and values of this schema can be customized if needed,
depending on specific cases.

Compatible schema are defined in [fw-modality-classficiation.yaml](./classification_profiles/fw-modality-classification.yaml).

If your Flywheel instance does not have a particular schema defined, it can be added
using the SDK. For instance, the **PT** (Positron Emission Tomography) classification schema
can be added as follow:

1. Get a **Flywheel** instance using the API key:

   ```python
   import flywheel
   client = flywheel.Client('<API-KEY>')
   ```

2. Load Classification schema from the provided **yaml** file and get the PT classification

   ```python
   import yaml
   
   classification_yaml = './classification_profiles/fw-modality-classification.yaml'
   with open(classification_yaml, 'r') as file:
      classification_schema = yaml.safe_load(file)
   
   pt_schema = classification_schema['PT']
      
   ```

3. Replace the PT Modality classification schema:

   ```python
   pt_modality = flywheel.Modality(classification=pt_schema)
   client.replace_modality('PT', pt_modality)
   ```

## Contributing

Profiles are stored under the [profiles](./classification_profiles) directory.

The default profile is [`main.yaml`](./classification_profiles/main.yaml), which
includes the other profiles defined in the same folder.

### Pre-requisites

This project uses `poetry` to manage virtual environments, and `pre-commit`
to perform linting and validation on scripts and profiles.

* Install [poetry](https://python-poetry.org/docs/#installation)
* Install [pre-commit](https://pre-commit.com/)

### Making a change

1. Initialize virtual environment with `poetry install`
2. Install pre-commit hooks with `pre-commit install`
3. Change to a new branch with a helpful name, e.g.
`git checkout -b <helpful_name>`
4. Make your relevant changes
5. Create a merge request.

### Releasing a new version

CI will automatically create a release Merge Request if you run a pipeline on `main`
with the variable `RELEASE` set to the desired version.

As long as that MR looks okay, go ahead and merge it, that will do the following:

1. Tag the repo with the desired release tag
2. Create a Release with a changelog from the merged MR titles
3. Create a downstream job to update the classification-profiles submodule on
   [file-classifier](https://gitlab.com/flywheel-io/flywheel-apps/file-classifier)

The downstream job in `3` will create an MR on file-classifier, which you can then merge
and release in the same way (via a `RELEASE` pipeline)

## Flywheel core-api dependencies

### core-api modalities.bson

Flywheel core ships with a default modality schema that is aligned with the schema
defined in `classification_profiles/fw-modality-classification.yaml`.

When modifying the classification schema, the `modalities.bson` file needs to be
updated in core-api as well. Until CI is in place for generating it, the following
steps can be used to update it:

* Update the fw-modality-classification.yaml file
* Run the following command to update the modalities.bson file found at <https://gitlabcom/flywheel-io/product/backend/core-api/-/blob/master/data/init_db/modalities.bson>:

   ```bash
   poetry run python bin/yaml2bson.py \
     --yaml classification_profiles/fw-modality-classification.yaml \
     --bson path/to/current/core-api/modalities.bson
   ```

* Check that the newly generated .bson file called `modalities_updated.bson` is
looking good and move to make that update to the core-api repo.

## Adding New Ground Truth Classification Profiles

This process is necessary only when you encounter a new classification that is not
covered by the current classification profiles.

To add a new ground truth header for testing classification profile changes, follow
these steps:

1) Download the file header using the `bin/download_headers.py` script. This script
will download the header of the file and save it in the `tests/ground_truth/unsorted/`
directory.

2) Move the header file to the appropriate directory after identifying the
classification.

3) Add the ground truth classification for the file in the
`/tests/ground_truth/assertions.csv` file.

At this point, the unit tests should recognize the new ground truth header and
validate the current classification profiles against it. Alternatively, you can run
the `/tests/ground_truth/debug_tests.py` script to obtain additional information
about the classification of the ground truth files.
