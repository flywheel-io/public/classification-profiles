#!/usr/bin/env python3
"""Script to convert modalities BSON to YAML file compatible with fw-modality-classification.yaml.

modalities.bson is defined in core-api and is used to define the modalities. It can be
found at https://gitlab.com/flywheel-io/product/backend/core-api/-/blob/master/data/init_db/modalities.bson
"""

from argparse import ArgumentParser

import bson
import yaml


def convert_bson_to_yaml(bson_file, yaml_file, raw=False):
    """Convert the modalities BSON defined in core-api to YAML."""
    with open(bson_file, "rb") as fp:
        bson_data = fp.read()
    bson_data_l = bson.decode_all(bson_data)

    if not raw:
        res = {}
        for d in bson_data_l:
            if d.get("active", False) is True:  # Only include active modalities
                res[d["_id"]] = d.get("classification", {})
    else:
        res = bson_data_l

    with open(yaml_file, "w") as yaml_file:
        yaml.dump(res, yaml_file, default_flow_style=False)


if __name__ == "__main__":
    parser = ArgumentParser(description="Convert modalities.bson to YAML")
    parser.add_argument("--bson", help="Path to modalities.bson")
    parser.add_argument("--yaml", help="Path to output YAML file")
    parser.add_argument("--raw", help="Keep the raw BSON data", action="store_true")

    args = parser.parse_args()

    convert_bson_to_yaml(args.bson, args.yaml, raw=args.raw)
