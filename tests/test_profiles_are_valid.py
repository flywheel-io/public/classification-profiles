from pathlib import Path

import pytest
from fw_classification.classify import Profile

PROFILES_ROOT = Path(__file__).parents[1] / "classification_profiles"


def collect_profile_files():
    # Recursively get all .yaml and .yml files, while ignoring __init__.py and fw-modality-classification.yaml
    return [
        f
        for f in PROFILES_ROOT.rglob("*")
        if f.is_file()
        and f.suffix in [".yaml", ".yml"]
        and f.name not in ["fw-modality-classification.yaml"]
    ]


@pytest.mark.parametrize("profile_file", collect_profile_files())
def test_validate_profiles(profile_file):
    profile_path = PROFILES_ROOT / profile_file
    profile = Profile(profile_path)
    assert not profile.errors
