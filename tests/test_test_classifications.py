"""Module to test the test_classifications module.

In test_classifications we check that the classification profiles work properly by
running a bunch of JSONs (containing DICOM headers) through those profiles and making
sure their classifications match the ground truth (defined in the
"ground_truth/assertions.csv" file).

With this test we test that the comparisons we make to check if the classification
matches what we expect works properly.
"""
import logging
from copy import deepcopy

import dotty_dict
import pandas as pd

from .test_classifications import matching_classification, unexpected_classification

EXPECTED_CLASSIFICATION = pd.Series(
    data={
        "classification_key": "ScanType,Feature,Feature",
        "classification_value": "Test,This,Passes",
    }
)
BASE_CLASSIFICATION = {
    "file": {
        "classification": {
            "ScanType": "Test",
            "Feature": ["This"],
        },
    },
}

log = logging.getLogger(__name__)


def test_matching_classification_matches():
    """Check that when the classification matches the expectations, it returns True."""
    passes_classification = dotty_dict.Dotty(dictionary=deepcopy(BASE_CLASSIFICATION))
    passes_classification["file.classification.Feature"].append("Passes")
    assert matching_classification(passes_classification, EXPECTED_CLASSIFICATION)


def test_matching_classification_no_match(caplog):
    """Check that when the classification does not match the expectations, it returns False."""
    fails_classification = dotty_dict.Dotty(dictionary=deepcopy(BASE_CLASSIFICATION))
    fails_classification["file.classification.Feature"].append("Fails")
    assert not matching_classification(fails_classification, EXPECTED_CLASSIFICATION)
    assert (
        "Expected value: 'Passes' does not match actual value: '['This', 'Fails']' for key: 'Feature'"
        in caplog.text
    )


def test_unexpected_classification_exists():
    """Check that when the classification exists in the list, it returns True."""
    exists_classification = dotty_dict.Dotty(dictionary=deepcopy(BASE_CLASSIFICATION))
    exists_classification["file.classification.Feature"].append("This")
    assert unexpected_classification(exists_classification, EXPECTED_CLASSIFICATION)


def test_unexpected_classification_no_exists(caplog):
    """Check that when the classification does not exist in the list, it returns False."""
    no_exists_classification = dotty_dict.Dotty(
        dictionary=deepcopy(BASE_CLASSIFICATION)
    )
    no_exists_classification["file.classification.Feature"].append("NotFound")
    assert not unexpected_classification(
        no_exists_classification, EXPECTED_CLASSIFICATION
    )
    assert (
        "Classification Mismatch: Key/Value pair (key: Feature: value: NotFound) not in expected values (['Test', 'This', 'Passes'])"
        in caplog.text
    )


def test_unexpected_classification_mismatch_length(caplog):
    """Check the scenario where the length of classification_key and classification_value does not match."""
    mismatch_classification = pd.Series(
        data={
            "classification_key": "ScanType,Feature",
            "classification_value": "Test,This,Passes",
        }
    )
    assert not unexpected_classification(BASE_CLASSIFICATION, mismatch_classification)
    assert (
        "Classification mismatch. Reason: len(['ScanType', 'Feature']) != len(['Test', 'This', 'Passes'])"
        in caplog.text
    )
